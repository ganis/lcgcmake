This file describes how to install generators to MCGenerators trees:

1. Download new authors tarfile to local source tarfile repository:

    a. Obtain sftnight AFS token;

    b. Download source tarball into `/afs/cern.ch/sw/lcg/external/tarFiles/MCGeneratorsTarFiles`;

2. Add package definition to requested version of LCGCMT release: `cmake/toolchain/heptools-<release>.cmake`;

3. Try build on local node and run GENSER test for the given package;

4. Commit changes into SVN;

5. FOR THE LCG TREES up to 67c:

    Go to https://ecsft.cern.ch/ and submit jobs for all requested releases;
    Don't install more then one full release at once!
   
    FOR THE LCG TREES 68 and higher: continue as below:

6. Go to https://phsft-jenkins.cern.ch/job/lcg_release_generators/
   or to the main Jenkins page https://phsft-jenkins.cern.ch/ and select lcg_release_generators
   This requires permission (now it is done through the GENSER e-group)

7. Login to Jenkins. This requires a Jenkins account, to be created once (ask Patricia).

8. Select "Build with parameters" and build generator. After the successful build there will
    be newly created RPMs in `/afs/cern.ch/sw/lcg/tmp/incoming_rpms`.

    NOTE: as of 17.03.2015 there is no Jenkins procedure to substitute a generator version
    already installed (to repair it for example). Such generator versions are now
    ignored (rpms not copied or not created)

9. Login as sftnight and copy rpms from `/afs/cern.ch/sw/lcg/tmp/incoming_rpms`
   to `/afs/cern.ch/sw/lcg/external/rpms/lcg/`

10. Update rpm database (use `createrepo` or `createrepo_c`) (takes about 10 mins):

    `createrepo_c --workers=20 /afs/cern.ch/sw/lcg/external/rpms/lcg/`

11. Install to AFS

    a. Prepare environment

            export MYSITEROOT=/afs/cern.ch/sw/lcg/releases/

    b. Install

       List rpms

            /afs/cern.ch/sw/lcg/app/spi/tools/LCGRPM/install/lcg_install.sh list | grep LCG_generators_74root6

       Install

            /afs/cern.ch/sw/lcg/app/spi/tools/LCGRPM/install/lcg_install.sh --rpmupdate install LCG_generators_74root6_x86_64_slc6_gcc48_opt-1.0.0-75

       NOTE: this installs the generator created in the last Jenkins job and its dependencies.

       If there were earlier Jenkins jobs with independent generators, they are to be installed

       one by one as below. Eventually this could be changed in future.

       If uninstalled generators remain or if an error occurred try to install target package

       (LCG_74_root6_lhapdf6_6.1.4_x86_64_slc6_gcc48_opt-1.0.0-11 for example)

       instead of full generators set (LCG_generators_74_root6_x86_64_slc6_gcc48_opt-1.0.0-75)

12. If the installation is successfull, mail to <mailto:genser-announce@cern.ch> like this:
    > Dear colleagues,

    > New Tauola++ version - Tauola++ 1.1.4 has been installed into MCGenerators lcgcmt trees:


    >   \- MCGenerators_lcgcmt65

    >   \- MCGenerators_lcgcmt65a

    >   \- MCGenerators_lcgcmt66

    > \---

    > Best regards,

    >  GENSER
