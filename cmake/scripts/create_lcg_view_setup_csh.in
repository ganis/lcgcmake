#---Source this script to setup the complete environment for this LCG view
#   Generated: @date@
 #---Get the location this script (thisdir)
set ARGS=($_)
set LSOF=`env PATH=/usr/sbin:${PATH} which lsof`
set thisfile="`${LSOF} -w +p $$ | grep -oE '/.*setup.csh'  `"
if ( "$thisfile" != "" && -e ${thisfile} ) then
   set thisdir="`dirname ${thisfile}`"
else if ("$ARGS" != "") then
   set thisdir="`dirname ${ARGS[2]}`"
endif
set thisdir="`readlink -f ${thisdir}`"

#---First the compiler
if ( -e @compilerlocation@/setup.csh ) then
   source @compilerlocation@/setup.csh
endif
#---then the rest...
if ($?PATH) then
   setenv PATH ${thisdir}/bin:${PATH}
else
   setenv PATH ${thisdir}/bin
endif
if ($?LD_LIBRARY_PATH) then
   setenv LD_LIBRARY_PATH ${thisdir}/lib64:${thisdir}/lib:${LD_LIBRARY_PATH}
else
   setenv LD_LIBRARY_PATH ${thisdir}/lib64:${thisdir}/lib
endif

set pyversion=`ls -l ${thisdir}/bin | awk '{ print $9 }' | grep '^python[0-9]\.[0-9]\+$' | head -1`
set py_path=${thisdir}/lib/$pyversion/site-packages
if ($?PYTHONPATH) then
   setenv PYTHONPATH ${thisdir}/lib:${py_path}:${PYTHONPATH}
else
   setenv PYTHONPATH ${thisdir}/lib:${py_path}
endif

if ($?CMAKE_PREFIX_PATH) then
   setenv CMAKE_PREFIX_PATH ${thisdir}:${CMAKE_PREFIX_PATH}
else
   setenv CMAKE_PREFIX_PATH $thisdir
endif
#---then ROOT
if ( -e $thisdir/bin/root ) then
   set rootdir=`readlink $thisdir/bin/root`
   set rootdir=`dirname $rootdir`
   set rootdir=`dirname $rootdir`
   setenv ROOTSYS $rootdir
   if ($?ROOT_INCLUDE_PATH) then
      setenv ROOT_INCLUDE_PATH ${thisdir}/include:${ROOT_INCLUDE_PATH}
   else
      setenv ROOT_INCLUDE_PATH ${thisdir}/include
   endif
   if ($?JUPYTER_PATH) then
      setenv JUPYTER_PATH ${thisdir}/etc/notebook:${JUPYTER_PATH}
   else
      setenv JUPYTER_PATH ${thisdir}/etc/notebook
   endif
endif
#---then Geant4
if ( -e $thisdir/bin/geant4-config ) then
   foreach line ( "`$thisdir/bin/geant4-config --datasets`" )
      set vars = ($line)
      setenv $vars[2] $vars[3]
   end
endif
#---then JAVA
if ( Darwin == `uname` && -e /usr/libexec/java_home ) then
   setenv JAVA_HOME `/usr/libexec/java_home`
else if ( -e $thisdir/bin/java ) then
   set java_home=`readlink $thisdir/bin/java`
   set java_home=`dirname $java_home`
   set java_home=`dirname $java_home`
   setenv JAVA_HOME $java_home
   setenv LD_LIBRARY_PATH ${java_home}/jre/lib/amd64:${LD_LIBRARY_PATH}
endif
#---then PYTHON
if ( -e $thisdir/bin/python ) then
   set python_home=`readlink $thisdir/bin/python`
   set python_home=`dirname $python_home`
   set python_home=`dirname $python_home`
   setenv PYTHONHOME $python_home
else if ( -e $thisdir/bin/python3 ) then
   set python_home=`readlink $thisdir/bin/python3`
   set python_home=`dirname $python_home`
   set python_home=`dirname $python_home`
   setenv PYTHONHOME $python_home
endif

#---then SPARK
if ( -e $thisdir/bin/pyspark ) then
   set spark_home=`readlink $thisdir/bin/pyspark`
   set spark_home=`dirname $spark_home`
   set spark_home=`dirname $spark_home`
   setenv SPARK_HOME $spark_home
   if ( $pyversion =~ python2* ) then
       set pysparkpython=$PYTHONHOME/bin/python
       setenv PYSPARK_PYTHON $pysparkpython
   else if ( $pyversion =~ python3* ) then
       set pysparkpython=$PYTHONHOME/bin/python3
       setenv PYSPARK_PYTHON $pysparkpython
   endif
endif
#---then BLAS
if ( -e $thisdir/lib/libBLAS.a ) then
    setenv BLAS_LIBS `readlink $thisdir/lib/libBLAS.a`
endif 
#---then LAPACK
if ( -e $thisdir/lib/libLAPACK.a ) then
    setenv LAPACK_LIBS `readlink $thisdir/lib/libLAPACK.a`
endif
#---then OCTAVE
if ( -e $thisdir/bin/octave ) then
   set octave_bin=`readlink $thisdir/bin/octave`
   set octave_bin=`dirname $octave_bin`
   set octave_home=`dirname $octave_bin`
   setenv OCTAVE_BINDIR $octave_bin
   setenv OCTAVE_HOME $octave_home
   setenv OCTAVE_LINK_DEPS "-lfreetype -lz -lGL -lGLU -lfontconfig -lfreetype -lX11 -lncurses -lpcre -ldl -lgfortran -lm -lquadmath -lutil -lm -L$thisdir/lib $BLAS_LIBS $LAPACK_LIBS"
   setenv OCT_LINK_DEPS $OCTAVE_LINK_DEPS
endif
#---then HBASE
if ( -e $thisdir/bin/hbase ) then
   set hbase_home=`readlink $thisdir/bin/hbase`
   set hbase_home=`dirname $hbase_home`
   set hbase_home=`dirname $hbase_home`
   setenv HBASE_HOME $hbase_home
endif
#---then Jupyter
if ( -e $thisdir/bin/jupyter ) then
   if ($?JUPYTER_PATH) then
      setenv JUPYTER_PATH ${thisdir}/share/jupyter:${JUPYTER_PATH}
   else
      setenv JUPYTER_PATH ${thisdir}/share/jupyter
   endif
endif
#---then R
if ( -e $thisdir/bin/R ) then
   setenv R_HOME `echo "cat(Sys.getenv('R_HOME'))" | $thisdir/bin/R --vanilla --slave`
   if ($?ROOT_INCLUDE_PATH) then
       setenv ROOT_INCLUDE_PATH ${ROOT_INCLUDE_PATH}:`echo "cat(Sys.getenv('R_INCLUDE_DIR'))" | $thisdir/bin/R --vanilla --slave`
   else
       setenv ROOT_INCLUDE_PATH `echo "cat(Sys.getenv('R_INCLUDE_DIR'))" | $thisdir/bin/R --vanilla --slave`
   endif
   setenv ROOT_INCLUDE_PATH ${ROOT_INCLUDE_PATH}:`echo "cat(find.package('RInside'))" | $thisdir/bin/R --vanilla --slave`/include
   setenv ROOT_INCLUDE_PATH ${ROOT_INCLUDE_PATH}:`echo "cat(find.package('Rcpp'))" | $thisdir/bin/R --vanilla --slave`/include
endif
#---then Valgrind
if ( -e $thisdir/bin/valgrind ) then
   setenv VALGRIND_LIB ${thisdir}/lib/valgrind
endif
#---then Delphes
if ( -e $thisdir/bin/DelphesHepMC ) then
   set delphes_dir=`readlink $thisdir/bin/DelphesHepMC`
   set delphes_dir=`dirname $delphes_dir`
   set delphes_dir=`dirname $delphes_dir`
   setenv DELPHES_DIR $delphes_dir
endif
#---then Pythia8
if ( -e $thisdir/bin/pythia8-config ) then
   set pythia8_dir=`readlink $thisdir/bin/pythia8-config`
   set pythia8_dir=`dirname $pythia8_dir`
   set pythia8_dir=`dirname $pythia8_dir`
   setenv PYTHIA8 $pythia8_dir
   setenv PYTHIA8DATA ${PYTHIA8}/share/Pythia8/xmldoc
endif
#---then Graphviz
if ( -e $thisdir/bin/dot ) then
   set graphviz_dir=`readlink $thisdir/bin/dot`
   set graphviz_dir=`dirname $graphviz_dir`
   set graphviz_dir=`dirname $graphviz_dir`
   setenv GVBINDIR $graphviz_dir/lib/graphviz
endif
#---then Go
if ( -e $thisdir/bin/go ) then
   set go_dir=`readlink $thisdir/bin/go`
   setenv GOROOT $go_dir
endif
#---then gophernotes
if ( -e $thisdir/bin/go ) then
   set gophernotes_bindir=`readlink $thisdir/bin/gophernotes`
   set gophernotes_dir=`dirname $gophernotes_bindir`
   set gophernotes_dir=`dirname $gophernotes_dir`
   if ($?GOPATH) then
      setenv GOPATH $gophernotes_dir:${GOPATH}
   else
      setenv GOPATH $gophernotes_dir
   endif
   if ($?GOBIN) then
      setenv GOBIN $gophernotes_bindir:${GOBIN}
   else
      setenv GOBIN $gophernotes_bindir
   endif
endif

if ( -e $thisdir/lib/libQt5Gui.so ) then
   set qt_dir=`readlink $thisdir/lib/libQt5Gui.so`
   set qt_dir=`dirname $qt_dir`
   set qt_dir=`dirname $qt_dir`
   setenv QT_PLUGIN_PATH $qt_dir/plugins
endif

if ( -f $thisdir/etc/fonts/fonts.conf ) then
    setenv FONTCONFIG_PATH $thisdir/etc/fonts
endif

#---then tensorflow
if ( -f $thisdir/lib/python2.7/site-packages/tensorflow/libtensorflow_framework.so ) then
    setenv LD_LIBRARY_PATH $thisdir/lib/python2.7/site-packages/tensorflow:$thisdir/lib/python2.7/site-packages/tensorflow/contrib/tensor_forest:$thisdir/lib/python2.7/site-packages/tensorflow/python/framework:${LD_LIBRARY_PATH}
endif

#---then PKG_CONFIG_FILE
if ($?PKG_CONFIG_PATH) then
   setenv PKG_CONFIG_PATH ${thisdir}/lib/pkgconfig:${PKG_CONFIG_PATH}
else
   setenv PKG_CONFIG_PATH ${thisdir}/lib/pkgconfig
endif


